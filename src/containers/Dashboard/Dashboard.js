import React, { Component } from 'react';
import {
  withStyles, Paper, List, ListItem, ListItemText, Typography, Button, Dialog, DialogTitle, DialogContent, DialogActions, CircularProgress, Select, MenuItem, InputLabel, FormControl,
} from '@material-ui/core';
import {
  Add,
} from '@material-ui/icons';
import { AppContext } from '../../contexts/appContext';
import FriendList from '../FriendList/FriendList';
import { TextField, PartyItem } from '../../components';
import Chat from '../Chat/Chat';


const styles = () => ({
  wrapper: {
    width: 'calc(100% - 268px)',
    minHeight: '100%',
  },
  dashboardPaper: {
    display: 'inline-block',
    verticalAlign: 'top',
    width: '100%',
    position: 'relative',
    textAlign: 'left',
    padding: 24,
  },
  addButton: {
    position: 'absolute',
    bottom: 24,
    right: 24,
  },
  offerDetail: {
    padding: '24px 48px',
  },
  naverMap: {
    width: '100%',
    height: '552px',
    marginBottom: 24,
  },
});

type Props = {
  classes: any,
};

class Dashboard extends Component<Props> {
  static contextType = AppContext

  state = {
    showAddOfferDialog: false,
    expand: null,
    addressesNearLocation: [],
    selectedLocation: '',
    offerFilterKeyword: '',
    content: '',
    title: '',
    hashtags: '',
    showChatDialog: false,
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(async (position) => {
      this.currentUserPosition = new naver.maps.LatLng(position.coords.latitude, position.coords.longitude);
      this.setState({
        currentUserAddress: (await this.getAddressesNearLocation(this.currentUserPosition))[0].split(' ').slice(0, 2).join(' '),
      });
    }, (err) => {
      console.log(err);
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.showAddOfferDialog && this.state.showAddOfferDialog) {
      setTimeout(() => {
        /* global naver */
        const currentLocation = this.currentUserPosition ? this.currentUserPosition : new naver.maps.LatLng(37.5666035, 126.9783868);
        this.naverMap = new naver.maps.Map('naverMap', {
          center: currentLocation,
          zoom: 20,
        });
        this.naverMapMarker = new naver.maps.Marker({
          position: currentLocation,
          map: this.naverMap,
        });
        naver.maps.Event.addListener(this.naverMap, 'click', (e) => {
          this.naverMapMarker.setPosition(e.latlng);
          this.calculateAddressesFromLocation(e.latlng);
        });
        window.naverMapMarker = this.naverMapMarker;

        this.calculateAddressesFromLocation(currentLocation);
      }, 100);
    }
  }

  calculateAddressesFromLocation = async (location) => {
    const { selectedLocation } = this.state;
    const addressesNearLocation = await this.getAddressesNearLocation(location);
    this.setState({
      addressesNearLocation,
      selectedLocation: addressesNearLocation.includes(selectedLocation) ? selectedLocation : '', // 현재 선택된 주소가 새 목록에 없을 경우 초기화
    });
  }

  getAddressesNearLocation = (location) => {
    const tm128 = naver.maps.TransCoord.fromLatLngToTM128(location);
    return new Promise((resolve, reject) => {
      naver.maps.Service.reverseGeocode({
        location: tm128,
        coordType: naver.maps.Service.CoordType.TM128,
      }, (status, response) => {
        if (status === naver.maps.Service.Status.ERROR) {
          reject(alert('지도 API 오류입니다.'));
        }

        const { items } = response.result;


        const addressesNearLocation = [];

        for (let i = 0, ii = items.length, item; i < ii; i += 1) {
          item = items[i];
          if (!addressesNearLocation.includes(item.address)) { // 중복 검사
            addressesNearLocation.push(item.address);
          }
        }

        resolve(addressesNearLocation);
      });
    });
  }

  addOffer = async () => {
    try {
      const {
        title, content, meetAt, selectedLocation: address, hashtags,
      } = this.state;

      if (!title || !content || !meetAt || !address) {
        throw new Error('필수 필드를 입력하세요.');
      }

      const cover = document.getElementById('offerCover').files[0];
      const dataInObject = {
        title,
        content,
        meetAt,
        address,
        hashtags: hashtags ? hashtags.replace(/ /g, '') : null,
        cover,
        reservedAt: meetAt,
      };
      const formData = new FormData();

      Object.keys(dataInObject).map((key) => {
        formData.append(key, dataInObject[key]);
      });

      this.setState({
        addingOffer: true,
      });
      await this.context.actions.addOffer(formData);
      await this.context.actions.refreshOffers();
      this.setState({
        showAddOfferDialog: false,
        addingOffer: false,
        content: '',
        title: '',
        hashtags: '',
      });
    } catch (error) {
      this.setState({ addingOffer: false });
    }
  }

  mutateOffer = async (offer, action) => {
    try {
      this.setState({
        offerMutating: true,
      });

      await action(offer.id);
      await this.context.actions.refreshOffers();

      this.setState({
        offerMutating: false,
        expand: null,
      });
    } catch (error) {
      console.log(error);
      this.setState({
        offerMutating: false,
        expand: null,
      });
    }
  }

  openAddOfferModal = () => {
    this.setState({ showAddOfferDialog: true, meetAt: new Date().toISOString().slice(0, -8) });
  }

  enterChat = async (offer) => {
    const {
      actions: {
        enterChat,
      },
    } = this.context;

    this.setState({
      showChatDialog: true,
      chatInitialized: false,
    });

    await enterChat(offer.id);

    this.setState({
      chatInitialized: true,
    });
  }

  exitChat = () => {
    const {
      actions: {
        exitChat,
      },
    } = this.context;
    this.setState({
      showChatDialog: false,
    });
    exitChat();
  }

  render() {
    const {
      classes,
    } = this.props;
    const {
      showAddOfferDialog,
      addingOffer,
      expand,
      offerMutating,
      selectedLocation,
      addressesNearLocation,
      currentUserAddress,
      offerFilterKeyword,
      hashtags,
      title,
      content,
      meetAt,
      showChatDialog,
      chatInitialized,
    } = this.state;
    const {
      state: {
        joinedOffers,
        activeOffers: allOffers,
      },
      actions: {
        leaveOffer,
        joinOffer,
        deleteOffer,
      },
    } = this.context;

    const activeOffers = currentUserAddress ? [] : allOffers;
    const nearOffers = currentUserAddress ? allOffers.filter((offer) => {
      if (offer.address.includes(currentUserAddress)) {
        return true;
      }
      activeOffers.push(offer);
    }) : [];

    return (
      <>
        <div className={classes.wrapper} style={{ height: '100%' }}>
          <Paper className={classes.dashboardPaper}>
            <Typography variant="subtitle1" color="primary">
            현재 참여중인 파티
            </Typography>
            <List>
              {
              joinedOffers.length
              && joinedOffers.map(offer => (
                <PartyItem
                  key={offer.id}
                  offer={offer}
                  classes={classes}
                  expand={expand}
                  expandAction={() => this.setState({ expand: expand === offer.id ? null : offer.id })}
                  loading={offerMutating}
                  chattable
                  chatAction={() => this.enterChat(offer)}
                  action={offer.owned ? () => this.mutateOffer(offer, deleteOffer) : () => this.mutateOffer(offer, leaveOffer)}
                  actionText={offer.owned ? '삭제하기' : '탈퇴하기'}
                />
              ))
              || (
              <ListItem style={{ paddingLeft: 8 }}>
                <ListItemText primary="현재 참여중인 파티가 없습니다." secondary="아래 파티 목록에서 참여할 파티를 선택해 보세요." />
              </ListItem>
              )
            }
            </List>
            {
            !!nearOffers.length
            && <>
              <Typography variant="subtitle1" color="primary">
                현재 장소 근처의 파티
              </Typography>
              <List>
                {
                  nearOffers.map(offer => (
                    <PartyItem
                      key={offer.id}
                      offer={offer}
                      classes={classes}
                      expand={expand}
                      expandAction={() => this.setState({ expand: expand === offer.id ? null : offer.id })}
                      loading={offerMutating}
                      action={() => this.mutateOffer(offer, joinOffer)}
                      actionText="참여하기"
                    />
                  ))
                }
              </List>
            </>
          }
            <Typography variant="subtitle1" color="primary">
            파티 목록
            </Typography>
            <TextField
              style={{
                marginBottom: 12, marginTop: 12, width: 240,
              }}
              label="검색어 입력"
              value={offerFilterKeyword}
              onChange={event => this.setState({ offerFilterKeyword: event.target.value })}
            />
            <List>
              {
              activeOffers.length
              && activeOffers.filter(offer => `${offer.title} ${offer.address} ${offer.hashtags} ${new Date(offer.meetAt).toLocaleString()}`.includes(offerFilterKeyword)).map(offer => (
                <PartyItem
                  key={offer.id}
                  offer={offer}
                  classes={classes}
                  expand={expand}
                  expandAction={() => this.setState({ expand: expand === offer.id ? null : offer.id })}
                  loading={offerMutating}
                  action={() => this.mutateOffer(offer, joinOffer)}
                  actionText="참여하기"
                />
              ))
              || (
              <ListItem style={{ paddingLeft: 8 }}>
                <ListItemText primary="현재 참여 가능한 파티가 없습니다." secondary="우하단의 파티 추가 버튼을 누르면 파티를 생성하실 수 있습니다." />
              </ListItem>
              )
            }
            </List>
            <Button onClick={this.openAddOfferModal} className={classes.addButton} color="primary" variant="fab">
              <Add />
            </Button>
          </Paper>
          <Dialog
            open={showAddOfferDialog}
            onClose={() => this.setState({ showAddOfferDialog: false })}
            fullWidth
            maxWidth="sm"
          >
            <DialogTitle>
            새 파티 생성하기
            </DialogTitle>
            <DialogContent>
              <div id="naverMap" className={classes.naverMap} />
              <FormControl fullWidth>
                <InputLabel htmlFor="select-address">주소 선택</InputLabel>
                <Select
                  fullWidth
                  value={selectedLocation}
                  onChange={event => this.setState({ selectedLocation: event.target.value })}
                  style={{ marginBottom: 12 }}
                  inputProps={{
                    name: 'selectAddress',
                    id: 'select-address',
                  }}
                >
                  <MenuItem value="">
                    <em>선택해 주세요</em>
                  </MenuItem>
                  {
                addressesNearLocation.map(location => <MenuItem key={location} value={location}>{location}</MenuItem>)
              }
                </Select>
              </FormControl>
              <TextField
                fullWidth
                focusOnMount
                style={{ marginBottom: 12 }}
                label="파티 제목"
                value={title}
                onChange={event => this.setState({ title: event.target.value })}
              />
              <TextField
                fullWidth
                style={{ marginBottom: 12 }}
                label="파티 내용"
                value={content}
                onChange={event => this.setState({ content: event.target.value })}
              />
              <TextField
                fullWidth
                style={{ marginBottom: 12 }}
                type="datetime-local"
                label="만날 시간"
                InputLabelProps={{
                  shrink: true,
                }}
                value={meetAt}
                onChange={event => this.setState({ meetAt: event.target.value })}
              />
              <TextField
                fullWidth
                style={{ marginBottom: 12 }}
                label="해시태그 (쉼표로 구분)"
                value={hashtags}
                onChange={event => this.setState({ hashtags: event.target.value })}
              />
              <Typography color="primary" style={{ marginTop: 24, marginBottom: 8 }}>파티 커버이미지</Typography>
              <input type="file" id="offerCover" />
            </DialogContent>
            <DialogActions>
              <Button disabled={addingOffer} onClick={() => this.setState({ showAddOfferDialog: false })}>
              취소
              </Button>
              <Button disabled={addingOffer} color="primary" onClick={this.addOffer}>
                {
                addingOffer
                && <CircularProgress style={{ marginRight: 8 }} size={14} />
              }
              추가하기
              </Button>
            </DialogActions>
          </Dialog>
          <Dialog
            open={showChatDialog}
            disableBackdropClick={chatInitialized}
            fullWidth
            maxWidth="sm"
          >
            <DialogTitle>
             채팅
            </DialogTitle>
            <DialogContent style={{ textAlign: 'center' }}>
              {
                !chatInitialized
                && <CircularProgress />
                || <Chat />
              }
            </DialogContent>
            <DialogActions>
              {
                chatInitialized
                && (
                <Button onClick={this.exitChat}>
                나가기
                </Button>
                )
              }
            </DialogActions>
          </Dialog>
        </div>
        <FriendList />
        </>
    );
  }
}

export default withStyles(styles)(Dashboard);
