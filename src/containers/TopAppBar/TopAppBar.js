import React, { Component } from 'react';
import {
  AppBar, Toolbar, Typography, IconButton, withStyles, Popover, Button, CircularProgress,
} from '@material-ui/core';
import {
} from '@material-ui/icons';
import imageProvider from '../../helpers/imageProvider';
import { UserContext } from '../../contexts/userContext';
import apiClient, { apiUrl } from '../../helpers/apiClient';

const styles = theme => ({
  appBar: {
    backgroundColor: 'white',
    minHeight: 48,
    height: 48,
  },
  toolBar: {
    minHeight: 48,
    height: 48,
    textAlign: 'left',
    color: theme.palette.primary.main,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  logo: {
    height: 36,
  },
  userProfile: {
    borderRadius: '50%',
    width: 36,
    height: 36,
    overflow: 'hidden',
  },
  profileButton: {
    padding: 0,
  },
  profileMenuImage: {
    width: 140,
    height: 140,
  },
  hiddenInput: {
    position: 'fixed',
    width: 0,
    height: 0,
    left: -100,
    top: -100,
  },
  profileMenuButton: {
    position: 'relative',
    marginBottom: 24,
    overflow: 'hidden',
    padding: 0,
  },
});

type Props = {
  classes: {},
};

class TopAppBar extends Component<Props> {
  static contextType = UserContext

  state = {
    profileMenuAnchor: null,
  }

  onProfileMenuClick = (event) => {
    this.setState({
      profileMenuAnchor: event.target,
    });
  }

  onProfileMenuClose = () => {
    this.setState({
      profileMenuAnchor: null,
    });
  }

  uploadProfile = async () => { // 프로필을 업로드하고, 업로드 하는 동안 업로드 중임을 표시합니다.
    try {
      const image = document.getElementById('uploadProfile').files[0];

      const profile = new FormData();
      profile.append('profile', image);

      this.setState({
        uploading: true,
      });

      await apiClient.put('/profile', {
        auth: true,
        data: profile,
      });

      this.setState({
        uploading: false,
        timestamp: new Date().getTime(),
      });
    } catch (error) {
      console.log(error);
      this.setState({
        uploading: false,
      });
    }
  }

  render() {
    const { classes } = this.props;
    const { profileMenuAnchor, uploading, timestamp } = this.state;
    console.log(this.state);
    const { state: { loginStatus: { identity } }, actions: { logout } } = this.context;
    return (
      <>
        <AppBar color="primary" position="sticky" className={classes.appBar}>
          <Toolbar color="primary" className={classes.toolBar}>
            <img src={imageProvider.logo_phrase} alt="Logo" className={classes.logo} />
            <Typography className={classes.grow} />
            <IconButton onClick={this.onProfileMenuClick} className={classes.profileButton} color="inherit" aria-label="Option">
              <img src={`http://${apiUrl}/profiles/${identity}?timestamp=${timestamp}`} alt="profile" className={classes.userProfile} />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Popover
          id="long-menu"
          anchorEl={profileMenuAnchor}
          open={!!profileMenuAnchor}
          onClose={this.onProfileMenuClose}
          PaperProps={{
            style: {
              width: 200,
              marginTop: 40,
              padding: 24,
              textAlign: 'center',
            },
          }}
        >
          <Typography color="primary" variant="h6" style={{ marginBottom: 12 }}>{identity}</Typography>
          <IconButton
            style={{ background: uploading ? 'rgba(0,0,0,0.2)' : '' }}
            className={classes.profileMenuButton}
            onClick={() => { document.getElementById('uploadProfile').click(); }}
          >
            <img src={`http://${apiUrl}/profiles/${identity}?timestamp=${timestamp}`} alt="profile" className={classes.profileMenuImage} />
            {
              uploading // 업로드 중일 경우에 로딩중 표시
              && (
              <CircularProgress
                style={{
                  color: 'white', position: 'absolute', top: 20, left: 20,
                }}
                size={100}
              />
              )
            }
          </IconButton>
          <input type="file" id="uploadProfile" className={classes.hiddenInput} onChange={() => this.uploadProfile()} />
          <Button variant="contained" fullWidth color="primary" onClick={logout}>
            로그아웃
          </Button>
        </Popover>
      </>
    );
  }
}

export default withStyles(styles)(TopAppBar);
