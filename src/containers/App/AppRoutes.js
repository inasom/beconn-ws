import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Dashboard from '../Dashboard/Dashboard';

const routes = [
  {
    path: 'dashboard',
    component: Dashboard,
  },
];

type Props = {
  style: {},
};

const AppRouter = (props: Props) => {
  const { style } = props;
  return (
    <div style={{
      minHeight: '100%', width: '100%', display: 'flex', ...style,
    }}
    >
      <Switch>
        {routes.map((singleRoute) => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              key={singleRoute.path}
              path={`/${singleRoute.path}`}
              exact={exact}
              {...otherProps}
            />
          );
        })}
        <Route // 어떤 Route에도 해당되지 않을 시 Dashboard 컴포넌트로 fallback 합니다.
          component={Dashboard}
        />
      </Switch>
    </div>
  );
};

export default AppRouter;
