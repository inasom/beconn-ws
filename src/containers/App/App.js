import React, { Component } from 'react';
import TopAppBar from '../TopAppBar/TopAppBar';
import AppRouter from './AppRoutes';
import './global.css';
import AppContextProvider from '../../contexts/appContext';

type Props = {
  location: any
}
class App extends Component<Props> {
  state = {
  }

  render() {
    return (
      <>
        <AppContextProvider>
          <TopAppBar location={this.props.location} />
          <div style={{ minHeight: '100%', width: '100%' }} className="App">
            <AppRouter />
          </div>
        </AppContextProvider>
      </>
    );
  }
}

export default App;
