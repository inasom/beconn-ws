import React, { Component } from 'react';
import {
  withStyles, List, ListItem, ListSubheader, Avatar, Paper, ListItemText, IconButton, Menu, MenuItem, CircularProgress,
} from '@material-ui/core';
import {
  Send, Check, Close,
} from '@material-ui/icons';
import { AppContext } from '../../contexts/appContext';
import { apiUrl } from '../../helpers/apiClient';

type Props = {
  classes: {},
};

const styles = {
  friendListPaper: {
    width: 244,
    marginLeft: 24,
    verticalAlign: 'top',
    display: 'inline-block',
    height: '100%',
  },
  list: {
    textAlign: 'left',
  },
};

class FriendList extends Component<Props> {
  static contextType = AppContext

  state = {
    invitationalOffers: [],
    confirmingInviteList: [],
  }

  getInvitationalList = (friend, event) => {
    const {
      state: {
        joinedOffers,
      },
    } = this.context;

    const invitationalOffers = joinedOffers.filter(offer => offer.owned && !offer.joinedUsers.filter(user => user.id === friend.id).length); // 자신 소유의 Offer 중 해당 친구가 속해있지 않은 Offer들을 선택

    this.setState({
      inviteAnchor: event.target,
      invitationalOffers,
      invitingFriend: friend.id,
    });
  }

  closeInviteMenu = () => {
    this.setState({
      inviteAnchor: null,
      invitationalOffers: [],
      invitingFriend: null,
    });
  }

  inviteFriend = async (friend, offer) => {
    try {
      const {
        actions: {
          inviteFriend,
        },
      } = this.context;

      this.closeInviteMenu();
      this.setState({
        sendingInvite: friend.id,
      });

      await inviteFriend({ friendId: friend.id, offerId: offer.id });
    } catch (error) {
      console.log(error);
    }

    this.setState({
      sendingInvite: false,
    });
  }

  confirmInvite = async (offer, isConfirmed) => {
    const {
      actions: {
        confirmInvite,
      },
    } = this.context;
    const {
      confirmingInviteList: confirmingInviteListBefore,
    } = this.state;

    this.setState({
      confirmingInviteList: [...confirmingInviteListBefore, offer.id],
    });

    await confirmInvite({ offerId: offer.id, isConfirmed });

    const {
      confirmingInviteList: confirmingInviteListAfter,
    } = this.state;

    this.setState({
      confirmingInviteList: [...confirmingInviteListAfter.filter(confirmingOffer => confirmingOffer !== offer.id)],
    });
  }

  render() {
    const {
      classes,
    } = this.props;

    const {
      inviteAnchor,
      invitationalOffers,
      sendingInvite,
      confirmingInviteList,
      invitingFriend,
    } = this.state;

    const { state: { friendList, inviteList } } = this.context;

    return (
      <Paper className={classes.friendListPaper}>
        <List classes={{ root: classes.list }}>
          {
            !!inviteList.length
            && <>
              <ListSubheader>
              받은 파티 초대
              </ListSubheader>
              {
                inviteList.map(offer => (
                  <ListItem key={offer.id}>
                    <ListItemText primaryTypographyProps={{ style: { fontSize: 12 } }} primary={offer.title} />
                    {
                      confirmingInviteList.includes(offer.id)
                      && (
                      <IconButton style={{ padding: 6 }} color="primary" onClick={() => this.confirmInvite(offer, true)}>
                        <CircularProgress size={24} />
                      </IconButton>
                      )
                      || (
                      <>
                        <IconButton style={{ padding: 6 }} color="primary" onClick={() => this.confirmInvite(offer, true)}>
                          <Check />
                        </IconButton>
                        <IconButton style={{ padding: 6 }} color="secondary" onClick={() => this.confirmInvite(offer, false)}>
                          <Close />
                        </IconButton>
                      </>
                      )
                    }
                  </ListItem>
                ))
              }
            </>
          }
          <ListSubheader>
            친구목록
          </ListSubheader>
          {
            friendList.length
            && friendList.map(friend => (
              <ListItem key={friend.id}>
                <Avatar>
                  <img style={{ width: '100%' }} src={`http://${apiUrl}/profiles/${friend.identity}`} alt="profile" className={classes.userProfile} />
                </Avatar>
                <ListItemText primaryTypographyProps={{ style: { fontSize: 12 } }} primary={friend.identity} />
                <IconButton onClick={event => this.getInvitationalList(friend, event)}>
                  {
                  sendingInvite
                    ? sendingInvite === friend.id
                  && <CircularProgress size={24} />
                    : (
                      <Send />
                    )
                }
                </IconButton>
                <Menu
                  id="lock-menu"
                  anchorEl={inviteAnchor}
                  open={Boolean(inviteAnchor) && invitingFriend === friend.id}
                  onClose={this.closeInviteMenu}
                >
                  <MenuItem
                    disabled
                  >
                    파티에 초대하기...
                  </MenuItem>
                  {invitationalOffers.map(offer => (
                    <MenuItem
                      key={offer.id}
                      onClick={() => this.inviteFriend(friend, offer)}
                    >
                      {offer.title}
                    </MenuItem>
                  ))}
                </Menu>
              </ListItem>
            ))
            || (
            <ListItem>
              <ListItemText secondary="안타깝게도, 귀하에게는 친구가 없습니다.." />
            </ListItem>
            )
          }
        </List>
      </Paper>
    );
  }
}

export default withStyles(styles)(FriendList);
