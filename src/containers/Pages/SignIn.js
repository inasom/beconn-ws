import React, { Component } from 'react';
import {
  withStyles, Paper, Avatar, Typography, Button, CircularProgress, FormControl,
} from '@material-ui/core';
import classNames from 'classnames';
import {
  Lock,
} from '@material-ui/icons';
import { TextField } from '../../components';
import { UserContext } from '../../contexts/userContext';

const styles = theme => ({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 160,
  },
  signInPaper: {
    width: 400,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 48,
  },
  signInTitle: {
    marginBottom: 24,
  },
  lockAvatar: {
    marginBottom: 24,
    width: 48,
    height: 48,
    backgroundColor: theme.palette.primary.main,
  },
  toRegister: {
    textAlign: 'right',
    marginBottom: 24,
    cursor: 'pointer',
  },
  afterRegister: {
    textAlign: 'right',
    marginBottom: 24,
  },
  textField: {
    marginTop: 16,
    marginBottom: 16,
  },
  password: {
    '&>div>input': {
      fontFamily: 'Roboto, sans-serif',
    },
  },
  checkboxLabel: {
    padding: '0px 24px',
  },
  warning: {
    color: '#767676',
    paddingLeft: 24,
  },
  error: {
    color: theme.palette.primary.main,
  },
});

type Props = {
  classes: Object,
  history: {
    push: string => null,
  }
};

class SignIn extends Component<Props> {
  static contextType = UserContext

  state = {
    identity: '',
    password: '',
  }

  componentDidMount() {
    this.props.history.push('/');
  }

  handleKeyDown = (event) => {
    if (event.keyCode === 13) {
      this.login();
    }
  }

  login = () => {
    const { identity, password } = this.state;
    const { login } = this.context.actions;

    login({
      identity,
      password,
    });
  }

  render() {
    const { classes } = this.props;
    const { identity, password } = this.state;
    const { state: { loginStatus: { error, loading }, registerStatus: { success: registerSuccess } } } = this.context;
    return (
      <div className={classes.wrapper}>
        <Paper className={classes.signInPaper}>
          <Avatar className={classes.lockAvatar}>
            <Lock />
          </Avatar>
          <Typography variant="h5" className={classes.signInTitle}>로그인</Typography>
          {
            error
            && (
            <Typography className={classes.error}>
              { error.message }
            </Typography>
            )
          }
          <FormControl fullWidth>
            <TextField
              value={identity}
              onChange={event => this.setState({ identity: event.target.value })}
              className={classes.textField}
              label="아이디"
              onKeyDown={this.handleKeyDown}
            />
            <TextField
              value={password}
              onChange={event => this.setState({ password: event.target.value })}
              className={classNames(classes.textField, classes.password)}
              type="password"
              label="비밀번호"
              onKeyDown={this.handleKeyDown}
            />
            {
              registerSuccess
              && <Typography className={classes.afterRegister} color="primary">가입한 아이디로 로그인해 주세요.</Typography>
              || <Typography onClick={() => this.props.history.push('/register')} className={classes.toRegister} color="primary">회원가입</Typography>
            }
            <Button
              variant="contained"
              block="true"
              color="primary"
              onClick={this.login}
            >
              로그인
              {
                loading
                && <CircularProgress style={{ marginLeft: 8, marginTop: -4, color: 'white' }} size={16} />
              }
            </Button>
          </FormControl>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(SignIn);
