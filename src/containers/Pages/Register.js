import React, { Component } from 'react';
import { Redirect } from 'react-router';
import {
  withStyles, Paper, Avatar, Typography, Button, CircularProgress, FormControl, Checkbox,
} from '@material-ui/core';
import classNames from 'classnames';
import {
  Lock,
} from '@material-ui/icons';
import { TextField } from '../../components';
import { UserContext } from '../../contexts/userContext';

const styles = theme => ({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 160,
  },
  registerPaper: {
    width: 400,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 48,
  },
  registerTitle: {
    marginBottom: 24,
  },
  lockAvatar: {
    marginBottom: 24,
    width: 48,
    height: 48,
    backgroundColor: theme.palette.primary.main,
  },
  textField: {
    marginTop: 16,
    marginBottom: 16,
  },
  password: {
    '&>div>input': {
      fontFamily: 'Roboto, sans-serif',
    },
  },
  warning: {
    color: '#767676',
    paddingLeft: 24,
  },
  error: {
    color: theme.palette.primary.main,
  },
});

type Props = {
  classes: {},
  history: {
    push: string => null,
  }
};

class Register extends Component<Props> {
  static contextType = UserContext

  state = {
    identity: '',
    password: '',
    passwordChecker: '',
    company: false,
  }

  handleKeyDown = (event) => {
    if (event.keyCode === 13) {
      this.register();
    }
  }

  register = async () => {
    const {
      identity, password, passwordChecker, company,
    } = this.state;

    this.context.actions.register({
      identity, password, passwordChecker, company,
    });
  }

  render() {
    const { classes } = this.props;
    const {
      identity, password, passwordChecker, company,
    } = this.state;
    const { state: { registerStatus: { error, loading, success } } } = this.context;

    if (success) {
      return <Redirect to="/" />;
    }
    return (
      <div className={classes.wrapper}>
        <Paper className={classes.registerPaper}>
          <Avatar className={classes.lockAvatar}>
            <Lock />
          </Avatar>
          <Typography variant="h5" className={classes.registerTitle}>회원가입</Typography>
          {
            error
            && (
            <Typography className={classes.error}>
              { error.message }
            </Typography>
            )
          }
          <FormControl fullWidth>
            <TextField
              value={identity}
              onChange={event => this.setState({ identity: event.target.value })}
              className={classes.textField}
              label="아이디"
              onKeyDown={this.handleKeyDown}
            />
            <TextField
              value={password}
              onChange={event => this.setState({ password: event.target.value })}
              className={classNames(classes.textField, classes.password)}
              type="password"
              label="비밀번호"
              onKeyDown={this.handleKeyDown}
            />
            <TextField
              value={passwordChecker}
              onChange={event => this.setState({ passwordChecker: event.target.value })}
              className={classNames(classes.textField, classes.password)}
              type="password"
              label="비밀번호 확인"
              onKeyDown={this.handleKeyDown}
            />
            <Typography style={{ cursor: 'pointer' }} onClick={() => this.setState({ company: !company })}>
              <Checkbox style={{ padding: '24px 8px 24px 0' }} color="primary" checked={company} />사업자일 경우 체크
            </Typography>
            <Button style={{ marginTop: 24 }} variant="contained" block="true" color="primary" onClick={this.register}>가입하기 {loading && <CircularProgress style={{ marginLeft: 8, marginTop: -4, color: 'white' }} size={16} />}</Button>
          </FormControl>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(Register);
