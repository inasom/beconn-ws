import React, { Component } from 'react';
import {
  withStyles, List, ListItem, ListItemText, Button, Typography, OutlinedInput,
} from '@material-ui/core';
import {
  Send,
} from '@material-ui/icons';
import { AppContext } from '../../contexts/appContext';

const styles = theme => ({
  input: {
    width: '100%',
    display: 'flex',
  },
  people: {
    maxHeight: '64px',
    borderBottom: '1px solid #ddd',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  person: {
    padding: '4px 0px',
  },
  message: {
    padding: '2px 0px',
  },
  chat: {
    minHeight: 360,
    maxHeight: 360,
    overflow: 'auto',
  },
  inputComponent: {
    width: 'calc(100% - 64px)',
  },
  inputOutline: {
    borderRadius: '4px 0px 0px 4px',
  },
  inputButton: {
    borderRadius: '0px 4px 4px 0px',
    boxShadow: 'none',
  },
  writer: {
    marginRight: 8,
    fontWeight: 'bold',
    color: theme.palette.primary.main,
  },
});

type Props = {
  classes: {},
};

class Chat extends Component<Props> {
  static contextType = AppContext

  state = {
    chatMessage: '',
  }

  componentDidUpdate() {
    const {
      state: {
        messageTimestamp,
      },
    } = this.context;
    const chatRef = document.getElementById('chatContainer');

    if (messageTimestamp !== this.state.messageTimestamp) { // 새 메시지가 있을 경우 스크롤을 가장 아래로
      this.setState({ // eslint-disable-line
        messageTimestamp,
      });
      chatRef.scrollTo(0, chatRef.scrollHeight);
    }
  }

  getPeople = () => {
    const { classes } = this.props;
    const { state: { people } } = this.context;
    return people.map(person => (
      <ListItem key={person.userId} className={classes.person}>
        <ListItemText>
          <Typography>{person.identity}</Typography>
        </ListItemText>
      </ListItem>
    ));
  }

  getMessages = () => {
    /* eslint-disable react/no-array-index-key */
    const { classes } = this.props;
    const { state: { messages, people } } = this.context;
    const peopleNameMap = people.reduce((nameMap, person) => ({ ...nameMap, [person.userId]: person.identity }), {});
    return messages.map((message, idx) => (
      <ListItem key={idx} className={classes.message}>
        <ListItemText>
          <Typography>
            <span className={classes.writer}>{peopleNameMap[message.userId] || 'Unknown'}</span>{message.message}
          </Typography>
        </ListItemText>
      </ListItem>
    ));
    /* eslint-enable react/no-array-index-key */
  }

  sendMessage = () => {
    const { chatMessage } = this.state;
    const {
      actions: {
        sendMessage,
      },
    } = this.context;
    if (chatMessage) {
      sendMessage(chatMessage);
      this.setState({
        chatMessage: '',
      });
    }
  }

  handleChatInput = (event) => {
    this.setState({
      chatMessage: event.target.value,
    });
  }

  handleKeyDown = (event) => {
    if (event.keyCode === 13) {
      this.sendMessage();
    }
  }

  render() {
    const { classes } = this.props;
    const { chatMessage } = this.state;
    return (
      <>
        <List className={classes.people}>
          {
            this.getPeople()
          }
        </List>
        <List id="chatContainer" className={classes.chat}>
          {
            this.getMessages()
          }
        </List>
        <div className={classes.input}>
          <OutlinedInput
            variant="outlined"
            labelWidth={0}
            onKeyDown={this.handleKeyDown}
            value={chatMessage}
            inputProps={{ style: { padding: 11.5 } }}
            onChange={this.handleChatInput}
            classes={{
              root: classes.inputComponent,
              notchedOutline: classes.inputOutline,
            }}
          />
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={this.sendMessage}
            className={classes.inputButton}
          ><Send />
          </Button>
        </div>
      </>
    );
  }
}

export default withStyles(styles)(Chat);
