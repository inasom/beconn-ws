import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import PublicRoutes from './router';
import UserContextProvider from './contexts/userContext';

const history = createHistory();

ReactDOM.render(
  <UserContextProvider>
    <PublicRoutes history={history} />
  </UserContextProvider>, document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
