import superagent from 'superagent';

const methods = ['get', 'post', 'put', 'patch', 'del'];
const apiUrl = '52.79.84.133:3000';

function formatUrl(path) { // 유저가 전달한 API URL을 적절하게 변환합니다.
  if (!/https|http/.test(path)) {
    const adjustedPath = path[0] !== '/' ? `/${path}` : path;
    const protocol = process.env.NODE_ENV === 'production' ? '' : 'http://';
    return `${protocol}${apiUrl}/apis${adjustedPath}`;
  }
  return path;
}

class ApiClient {
  constructor() {
    methods.forEach((method) => {
      this[method] = (path, { params, data, auth } = {}) => new Promise((resolve, reject) => {
        const request = superagent[method](formatUrl(path));

        if (params) { // params가 데이터로 넘어왔을 경우 query에 추가
          request.query(params);
        }

        if (auth) { // auth가 필요한 action의 경우 Authorization 헤더에 accessToken을 함께 보냅니다.
          const { accessToken } = JSON.parse(localStorage.getItem('login-info'));
          request.set('Authorization', `Bearer ${accessToken}`);
        }

        if (data) { // data가 있을 경우 request body로 데이터 전송
          request.send(data);
        }

        request.then((success, failure) => {
          if (!failure && success) {
            resolve(success.body ? success.body : success);
          }
          reject(failure);
        }).catch((err) => {
          reject(err.body ? err.body : err);
        });
      });
      return this[method];
    });
  }
}

export default new ApiClient();


export { apiUrl };
