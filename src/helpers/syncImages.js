// images 디렉토리 내의 image 파일들을 읽어 imageProvider.js 파일로 내보내는 스크립트입니다. 터미널에서 node syncImages.js 형태로 사용.

const fs = require('fs');

const images = fs.readdirSync('../images').filter(x => x.includes('png'));
const ex = `{
  ${images.map(x => `${x.split('.png')[0].replace(/@|\./g, '_')}: require('../images/${x}'),`).join('\n  ')}
};
/* eslint-enable */\n`;
const res = `/* eslint-disable */
export default ${ex}`;
fs.writeFileSync('./imageProvider.js', res);
console.log('Successfully synced images. Check imageProvider.js for result.');
