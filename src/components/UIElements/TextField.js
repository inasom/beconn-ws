import React, { Component } from 'react';
import {
  TextField, withStyles,
} from '@material-ui/core';

const styles = theme => ({
  cssLabel: {
    '&$cssFocused': {
      color: theme.palette.primary.main,
    },
  },
  cssUnderline: {
    '&:after': {
      borderBottomColor: theme.palette.primary.main,
    },
  },
  cssFocused: {},
});

type Props = {
  classes: {},
  children: any,
  noPadding: boolean,
  focusOnMount: boolean,
};

class MyTextField extends Component<Props> {
  componentDidMount() {
    const { focusOnMount } = this.props;

    if (focusOnMount) {
      document.getElementById(`focusInput${focusOnMount}`).focus();
    }
  }

  render() {
    const {
      classes, children, noPadding, focusOnMount, InputLabelProps, InputProps, ...otherProps
    } = this.props;

    return (
      <TextField
        {...otherProps}
        InputLabelProps={{
          FormLabelClasses: {
            root: classes.cssLabel,
            focused: classes.cssFocused,
          },
          ...InputLabelProps,
        }}
        InputProps={
          focusOnMount ? {
            classes: {
              underline: classes.cssUnderline,
            },
            id: `focusInput${focusOnMount}`,
            ...InputProps,
          } : {
            classes: {
              underline: classes.cssUnderline,
            },
            ...InputProps,
          }
        }
      >
        {children}
      </TextField>
    );
  }
}

export default withStyles(styles)(MyTextField);
