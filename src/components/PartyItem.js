import React from 'react';
import {
  ListItem, Avatar, ListItemText, Collapse, Typography, Button, CircularProgress,
} from '@material-ui/core';
import {
  Message,
} from '@material-ui/icons';
import { apiUrl } from '../helpers/apiClient';

type Props = {
  offer: Object,
  loading: Boolean,
  action: Function,
  expand: String,
  expandAction: Function,
  classes: Object,
  actionText: String,
  chatAction: Function,
  chattable: Boolean,
}

const PartyItem = (props: Props) => {
  const {
    offer, loading, action, expand, expandAction, classes, actionText, chattable, chatAction,
  } = props;
  const hashTags = offer.hashtags !== 'null' && offer.hashtags !== 'undefined' ? offer.hashtags.split(',').map(tag => `#${tag}`).join(', ') : '등록된 해시태그 없음';
  return (
    <React.Fragment>
      <ListItem button onClick={expandAction}>
        <Avatar>
          <img src={`http://${apiUrl}/covers/${offer.id}.png`} alt="cover" style={{ width: 64 }} />
        </Avatar>
        <ListItemText primary={offer.title} secondary={`${offer.address}, ${new Date(offer.meetAt).toLocaleString()}, ${hashTags}`} />
      </ListItem>
      <Collapse in={expand === offer.id} className={classes.offerDetail} unmountOnExit>
        <Typography color="primary" variant="subtitle2">파티 제목</Typography>
        <Typography style={{ marginBottom: 12 }}>{offer.title}</Typography>

        <Typography color="primary" variant="subtitle2">파티 내용</Typography>
        <Typography style={{ marginBottom: 12 }}>{offer.content}</Typography>

        <Typography color="primary" variant="subtitle2">생성된 시간</Typography>
        <Typography style={{ marginBottom: 12 }}>{new Date(offer.createdAt).toLocaleString()}</Typography>

        <Typography color="primary" variant="subtitle2">만나는 시간</Typography>
        <Typography style={{ marginBottom: 12 }}>{new Date(offer.meetAt).toLocaleString()}</Typography>

        <Typography color="primary" variant="subtitle2">만나는 장소</Typography>
        <Typography style={{ marginBottom: 12 }}>{offer.address}</Typography>

        <Typography color="primary" variant="subtitle2">해시태그</Typography>
        <Typography style={{ marginBottom: 12 }}>{hashTags}</Typography>

        <Typography color="primary" variant="subtitle2">현재 참여중인 유저</Typography>
        <Typography style={{ marginBottom: 12 }}>{offer.joinedUsers.filter(user => user.join.confirm).map(user => user.identity).join(', ')}</Typography>
        <Button
          disabled={loading}
          onClick={action}
          variant="contained"
          color="primary"
        >
          {
            loading
            && <CircularProgress style={{ marginRight: 8, color: 'white' }} size={14} />
          }
          {actionText}
        </Button>
        {
          chattable
          && (
          <Button
            onClick={chatAction}
            variant="contained"
            color="primary"
            style={{ marginLeft: 8 }}
          >
            <Message style={{ marginRight: 8, fontSize: 16 }} /> 채팅하기
          </Button>
          )
        }
      </Collapse>
    </React.Fragment>
  );
};

export default PartyItem;
