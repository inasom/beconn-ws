import React, { Component, createContext } from 'react';
import apiClient from '../helpers/apiClient';


// Context를 생성하고, 생성한 Context의 Provider를 참조합니다.
const UserContext = createContext();
const { Provider } = UserContext;

// Flow를 사용해 Type Definition을 수행합니다.
type Props = {
  children: any,
  history: {
    push: string => null,
  }
}

export default class UserContextProvider extends Component<Props /* 위에서 선언한 Prop타입을 컴포넌트에서 참조합니다 */> {
  state = {
    loginStatus: {
    },
    registerStatus: {
    },
    accessToken: null,
  }

  actions = { // actions에서 Context의 value를 변경하기 위한 함수들을 정의합니다. 이 actions는 마찬가지로 Context를 통해 다른 컴포넌트로 전달합니다..
    login: async ({ identity, password }) => {
      try {
        this.setState({
          loginStatus: {
            loading: true,
          },
        });

        // 로그인 API를 요청하고, 응답을 loginResult에 저장합니다.
        const loginResult = await apiClient.post('/signin', {
          data: {
            identity,
            password,
          },
        });

        const { accessToken } = loginResult;
        const createdDate = new Date().getTime();

        localStorage.setItem('login-info', JSON.stringify({ accessToken, createdDate, identity }));

        this.setState({
          loginStatus: {
            ...loginResult,
            error: false,
            success: true,
            identity,
          },
        });
      } catch (error) { // 로그인 실패시 fallBack
        console.log(error);
        this.setState({
          loginStatus: {
            error: new Error('아이디 혹은 패스워드가 잘못되었습니다.'),
          },
        });
      }
    },
    register: async ({
      identity, password, passwordChecker, company,
    }) => {
      try {
        if (password !== passwordChecker) {
          this.setState({
            registerStatus: {
              error: new Error('확인 비밀번호가 일치하지 않습니다.'),
            },
          });

          return false;
        }

        this.setState({
          registerStatus: {
            loading: true,
            identity,
          },
        });

        await apiClient.post('/signup', {
          data: {
            identity,
            password,
            level: company ? 2 : 1,
          },
        });

        this.setState({
          registerStatus: {
            loading: false,
            success: true,
          },
        });
      } catch (error) {
        console.log(error);
        this.setState({
          registerStatus: {
            error: new Error('입력하신 정보에 문제가 있습니다.'),
          },
        });
      }
    },
    logout: () => {
      localStorage.removeItem('login-info');
      this.setState({
        loginStatus: {
        },
        registerStatus: {
        },
      });
    },
  }

  componentDidMount() {
    try {
      const savedLoginInfo = JSON.parse(localStorage.getItem('login-info'));
      console.log(savedLoginInfo);

      if (new Date().getTime() - savedLoginInfo.createdDate < 1000 * 60 * 60 * 24) { // 토큰이 발급되고 하루 이상 지났는지 체크 (토큰은 하루후에 만료되도록 설계됨)
        this.setState({
          loginStatus: {
            ...savedLoginInfo,
            error: false,
            success: true,
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { children } = this.props;
    return (
      <Provider value={{ state: { ...this.state }, actions: this.actions }}>{ children } </Provider> // 이 줄에서, Provider의 value를 현재 컴포넌트의 state와 action으로 정의합니다.
    );
  }
}

export {
  UserContext,
};
