import React, { Component, createContext } from 'react';
import io from 'socket.io-client';
import apiClient from '../helpers/apiClient';


// Context를 생성하고, 생성한 Context의 Provider를 참조합니다.
const AppContext = createContext();
const { Provider } = AppContext;

// Flow를 사용해 Type Definition을 수행합니다.
type Props = {
  children: Component | Object | Function,
  history: {
    push: string => null,
  }
}

export default class AppContextProvider extends Component<Props /* 위에서 선언한 Prop타입을 컴포넌트에서 참조합니다 */> {
  state = {
    joinedOffers: [],
    activeOffers: [],
    friendList: [],
    inviteList: [],
    socket: {},
    messages: [],
    people: [],
  }

  actions = {
    refreshOffers: async () => {
      const offers = (await apiClient.get('/offers', { auth: true })).receives;
      const { identity } = JSON.parse(localStorage.getItem('login-info'));
      const inviteList = [];
      let userId;

      const joinedOffers = offers.filter(offer => offer.joinedUsers.filter((user) => {
        if (user.identity === identity) {
          userId = user.id;
          if (!user.join.confirm) {
            inviteList.push(offer);
          }
          return user.join.confirm;
        }
      }).length).map(offer => ({ ...offer, owned: userId === offer.userId }));
      const activeOffers = offers.filter(offer => !offer.joinedUsers.filter(user => user.identity === identity && user.join.confirm).length);
      this.setState({
        joinedOffers,
        activeOffers,
        friendList: joinedOffers.reduce((friends, nextOffer) => friends.concat(nextOffer.joinedUsers.filter(user => !friends.filter(friend => friend.id === user.id).length && user.id !== userId)), []), // Array의 reduce method를 사용해 현재 가입된 offer에 존재하는 자신이 아닌 다른 모든 사용자의 목록을 불러옵니다.
        inviteList,
      });
    },
    addOffer: async formData => apiClient.post('/offers', { data: formData, auth: true }),
    joinOffer: async offerId => apiClient.post(`/offer/${offerId}/join`, { auth: true }),
    deleteOffer: async offerId => apiClient.del(`/offer/${offerId}`, { auth: true }),
    leaveOffer: async offerId => apiClient.del(`/offer/${offerId}/leave`, { auth: true }),
    confirmInvite: ({ offerId, isConfirmed }) => new Promise((resolve, reject) => {
      const { socket, accessToken } = this.state;
      socket.emit('inviteConfirm', accessToken, isConfirmed, offerId, async (result) => {
        if (result) {
          await this.actions.refreshOffers();
          resolve(result);
        } else {
          reject();
        }
      });
    }),
    inviteFriend: ({ friendId, offerId }) => new Promise((resolve, reject) => {
      const { socket, accessToken } = this.state;

      /* accessToken, friendId List, OfferId */
      socket.emit('inviteFriends', accessToken, [friendId], offerId, async (result) => {
        if (result) {
          await this.actions.refreshOffers();
          resolve();
        } else {
          reject();
        }
      });
    }),
    enterChat: offerId => new Promise((resolve, reject) => {
      const { socket, accessToken } = this.state;
      this.setState({
        messages: [],
      });

      /* accessToken, friendId List, OfferId */
      socket.emit('joinRoom', accessToken, offerId, async (result) => {
        if (result) {
          resolve();
        } else {
          reject();
        }
      });
    }),
    sendMessage: message => new Promise((resolve) => {
      const { socket, accessToken } = this.state;
      socket.emit('message', accessToken, message, () => {
        resolve();
      });
    }),
    exitChat: () => new Promise((resolve, reject) => {
      const { socket, accessToken } = this.state;
      socket.emit('leaveRoom', accessToken, async (result) => {
        if (result) {
          resolve();
        } else {
          reject();
        }
      });
    }),
  }

  componentDidMount() {
    this.actions.refreshOffers();

    const { accessToken } = JSON.parse(localStorage.getItem('login-info'));

    const socket = io.connect('http://52.79.84.133:3001');

    socket.emit('register', accessToken, () => {
      socket.on('joinUserToRoom', (identity, userList) => {
        this.updatePeople(userList);
      });

      socket.on('leaveUserFromRoom', (identity, userList) => {
        console.log(userList);
        this.updatePeople(userList);
      });

      socket.on('message', (message) => {
        // 채팅방에 들어가있는 상태에서 누가 메시지 치면 (자기 자신도 포함, 메시지 옴.)
        // message는 userId와 offerId message 필드를 갖고 있음 message에 전문이 포함됨
        this.addMessages(message);
      });

      socket.on('messages', (messages) => {
        this.addMessages(...messages.reverse());
      });

      /* 여기부터는 조인 등에 관련 */

      socket.on('shouldUpdate', () => {
        this.actions.refreshOffers();
      });

      socket.on('invite', () => {
        this.actions.refreshOffers();
      });
    });

    this.setState({
      socket,
      accessToken,
    });
  }

  addMessages = (...messages) => {
    const { messages: currentMessages } = this.state;
    this.setState({
      messages: [...currentMessages, ...messages],
      messageTimestamp: new Date().getTime(),
    });
  }

  updatePeople = people => this.setState({ people })

  render() {
    const { children } = this.props;
    return (
      <Provider value={{ state: { ...this.state }, actions: this.actions }}>{ children } </Provider> // 이 줄에서, Provider의 value를 현재 컴포넌트의 state와 action으로 정의합니다.
    );
  }
}

export {
  AppContext,
};
