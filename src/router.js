import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme, CssBaseline } from '@material-ui/core';
import App from './containers/App/App';
import SignIn from './containers/Pages/SignIn';
import Register from './containers/Pages/Register';
import { UserContext } from './contexts/userContext';

type Props = {
  history: History,
}

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2acb60',
      contrastText: 'white',
    },
  },
  config: {
    topbarHeight: 48,
    chatWidth: 280,
  },
  typography: {
    useNextVariants: true,
  },
});
class PublicRoutes extends Component<Props> {
  static contextType = UserContext

  render() {
    const { history } = this.props;
    const { success: loginSuccess } = this.context.state.loginStatus;
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Router history={history}>
          <Switch>
            {
                loginSuccess // 유저가 로그인 되었을 경우에 Route. Switch는 내부의 Route 중 가장 처음 조건에 맞는 Route만을 Render하는 기능을 하므로 로그인이 되었을 경우 App 컴포넌트 만이 렌더링됨.
                && (
                <Route
                  component={App}
                />
                )
              }
            <Route
              component={Register}
              path="/register"
            />
            <Route
              component={SignIn}
            />
          </Switch>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default PublicRoutes;
